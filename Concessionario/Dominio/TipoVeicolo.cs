﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concessionario.Dominio
{
    public enum TipoVeicolo
    {
        Autocarro,
        Autovettura,
        Motocarro,
        Motociclo
    }
}
