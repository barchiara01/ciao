﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concessionario.Dominio
{
    public class Veicolo
    {
        //proprietà prop tab tab
        //set privato così una volta creato non può cambiare

        //costruttore ctor tab tab
        public Veicolo(string marca, string modello, int cilindrata, int cavalli, decimal prezzo, byte ruote)
        {
            Marca = marca;
            Modello = modello;
            Cilindrata = cilindrata;
            Cavalli = cavalli;
            Prezzo = prezzo;
            Ruote = ruote;
        }
        
        
        
        public string Marca { get; private set; }
        public string Modello { get; private set; }

        public int Cilindrata { get; private set; }

        public int Cavalli { get; private set; }

        public decimal Prezzo { get; private set; }

        public byte Ruote { get; private set; }

        public TipoVeicolo Tipo { get; private set; } = TipoVeicolo.Autovettura; //definisce di default Autovettura

        public void CambiaTipoVeicolo(TipoVeicolo nuovotipo)
        {
            if (Tipo != nuovotipo)
            {
                Tipo = nuovotipo;
            }
        }

        public string MarcaEModello
        {
            get { return $"{Marca} {Modello}"; }
            // get { return string.Format"{0} {1}", Marca, Modello; }
            //  get { return Marca+ " "+ Modello; } troppe variabili coinvolte
        }
    }
}
