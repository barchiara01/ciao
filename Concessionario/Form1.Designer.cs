﻿namespace Concessionario
{
    partial class Form1
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.CB_elencoModelli = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.BT_esci = new System.Windows.Forms.Button();
            this.labelMarca = new System.Windows.Forms.Label();
            this.TB_marca = new System.Windows.Forms.TextBox();
            this.TB_Modello = new System.Windows.Forms.TextBox();
            this.labelModello = new System.Windows.Forms.Label();
            this.TB_Prezzo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // CB_elencoModelli
            // 
            this.CB_elencoModelli.FormattingEnabled = true;
            this.CB_elencoModelli.Location = new System.Drawing.Point(156, 117);
            this.CB_elencoModelli.Name = "CB_elencoModelli";
            this.CB_elencoModelli.Size = new System.Drawing.Size(538, 21);
            this.CB_elencoModelli.TabIndex = 0;
            this.CB_elencoModelli.SelectedIndexChanged += new System.EventHandler(this.CB_elencoModelli_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 32.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(22, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(672, 51);
            this.label1.TabIndex = 1;
            this.label1.Text = "GESTIONE CONCESSIONARIO";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(30, 120);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Selezionare il modello";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.TB_Prezzo);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.TB_Modello);
            this.groupBox1.Controls.Add(this.labelModello);
            this.groupBox1.Controls.Add(this.TB_marca);
            this.groupBox1.Controls.Add(this.labelMarca);
            this.groupBox1.Location = new System.Drawing.Point(31, 160);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(661, 203);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // BT_esci
            // 
            this.BT_esci.Location = new System.Drawing.Point(651, 415);
            this.BT_esci.Name = "BT_esci";
            this.BT_esci.Size = new System.Drawing.Size(75, 23);
            this.BT_esci.TabIndex = 4;
            this.BT_esci.Text = "Esci";
            this.BT_esci.UseVisualStyleBackColor = true;
            this.BT_esci.Click += new System.EventHandler(this.BT_esci_Click);
            // 
            // labelMarca
            // 
            this.labelMarca.AutoSize = true;
            this.labelMarca.Location = new System.Drawing.Point(22, 39);
            this.labelMarca.Name = "labelMarca";
            this.labelMarca.Size = new System.Drawing.Size(37, 13);
            this.labelMarca.TabIndex = 0;
            this.labelMarca.Text = "Marca";
            // 
            // TB_marca
            // 
            this.TB_marca.Location = new System.Drawing.Point(87, 36);
            this.TB_marca.Name = "TB_marca";
            this.TB_marca.Size = new System.Drawing.Size(100, 20);
            this.TB_marca.TabIndex = 1;
            // 
            // TB_Modello
            // 
            this.TB_Modello.Location = new System.Drawing.Point(87, 83);
            this.TB_Modello.Name = "TB_Modello";
            this.TB_Modello.Size = new System.Drawing.Size(100, 20);
            this.TB_Modello.TabIndex = 3;
            // 
            // labelModello
            // 
            this.labelModello.AutoSize = true;
            this.labelModello.Location = new System.Drawing.Point(22, 86);
            this.labelModello.Name = "labelModello";
            this.labelModello.Size = new System.Drawing.Size(44, 13);
            this.labelModello.TabIndex = 2;
            this.labelModello.Text = "Modello";
            // 
            // TB_Prezzo
            // 
            this.TB_Prezzo.Location = new System.Drawing.Point(87, 124);
            this.TB_Prezzo.Name = "TB_Prezzo";
            this.TB_Prezzo.Size = new System.Drawing.Size(100, 20);
            this.TB_Prezzo.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 127);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Prezzo";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(738, 450);
            this.Controls.Add(this.BT_esci);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CB_elencoModelli);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.ComboBox CB_elencoModelli;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button BT_esci;
        private System.Windows.Forms.TextBox TB_Prezzo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TB_Modello;
        private System.Windows.Forms.Label labelModello;
        private System.Windows.Forms.TextBox TB_marca;
        private System.Windows.Forms.Label labelMarca;
    }
}

