﻿using Concessionario.Dominio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Concessionario
{
    public partial class Form1 : Form
    {
        private List<Veicolo> catalogoVeicoli = new List<Veicolo>()
        {
            new Veicolo( "BMW", "Serie 1", 2000, 150, 44000,4),
            new Veicolo( "Fiat", "Panda", 1000, 75, 10000,4),
            new Veicolo( "Mercedes", "Classe A", 2000, 160, 50000,4),
            new Veicolo( "Fiat", "Doblo", 1200, 80, 18000,4),
            new Veicolo( "Renault", "Clio", 1200, 95, 20000,4)
        };


        public Form1()
        {
            InitializeComponent();

            /*var v = new Veicolo ( "BMW", "Serie 1", 2000, 150, 44000,4);

            catalogoVeicoli.Add(v);*/
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //ordina la lista per modello
            //restituisci la proprietà modello di v
            // CB_elencoModelli.DataSource = catalogoVeicoli.OrderBy(v => v.Modello);  (f)
            CB_elencoModelli.DataSource = catalogoVeicoli;

            CB_elencoModelli.DisplayMember = "MarcaEModello";
            
        }

        private void BT_esci_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void CB_elencoModelli_SelectedIndexChanged(object sender, EventArgs e)
        {
            var veicoloSelezionato = CB_elencoModelli.SelectedItem as Veicolo;  //è di tipo Veicolo ogni elemento della ComboBox, non object, è un altro modo di fare un cast
            TB_marca.Text = veicoloSelezionato.Marca;
            TB_Modello.Text = veicoloSelezionato.Modello;
            TB_Prezzo.Text = veicoloSelezionato.Prezzo.ToString("c", new CultureInfo("en-gb")); //currency fammi il to string assumendo che è un prezzo
        }


        //equivale a notazione v => v.Modello
        private string f(Veicolo v)
        {
            return v.Modello;
        }

    }
}
